<?php

/**
 * Myrouter index controller
 *
 */
class Stas_Myrouter_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        echo 'My Page';
        $this->renderLayout();
    }
}

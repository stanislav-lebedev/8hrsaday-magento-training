<?php

/**
 * My Controller Router
 *
 */
class Stas_Myrouter_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters(Varien_Event_Observer $observer)
    {
        $front = $observer->getEvent()->getFront();

        $front->addRouter('myrouter', $this);
    }

    /**
     * Validate and Match request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        if (!Mage::isInstalled()) {
            Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }

        $identifier = trim($request->getPathInfo(), '/');
        $standard = Mage::getStoreConfig('customroutersection/customroutergroup/identifier');


        if ($identifier !== $standard) {
            return false;
        }

        $request->setModuleName('myrouter')
            ->setControllerName('index')
            ->setActionName('index');

        return true;
    }
}

<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'bd_faq/group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bd_faq/group'))
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Group ID')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Group Name')
    ->addColumn('image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
        ), 'Group Image')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        ), 'Sort Order')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
        ), 'Is Group Active')
    ->setComment('Bigdrop FAQ Group Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'bd_faq/group_store'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bd_faq/group_store'))
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
        ), 'Group ID')
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Store ID')
    ->addIndex($installer->getIdxName('bd_faq/group_store', array('store_id')),
        array('store_id'))
    ->addForeignKey($installer->getFkName('bd_faq/group_store', 'group_id', 'bd_faq/group', 'group_id'),
        'group_id', $installer->getTable('bd_faq/group'), 'group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('bd_faq/group_store', 'store_id', 'core/store', 'store_id'),
        'store_id', $installer->getTable('core/store'), 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('FAQ Group To Store Linkage Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'bd_faq/group_customer_group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bd_faq/group_customer_group'))
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
    ), 'Group ID')
    ->addColumn('customer_group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Store ID')
    ->addIndex($installer->getIdxName('bd_faq/group_customer_group', array('customer_group_id')),
        array('customer_group_id'))
    ->addForeignKey($installer->getFkName('bd_faq/group_customer_group', 'group_id', 'bd_faq/group', 'group_id'),
        'group_id', $installer->getTable('bd_faq/group'), 'group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('bd_faq/group_customer_group', 'customer_group_id', 'customer/customer_group', 'customer_group_id'),
        'customer_group_id', $installer->getTable('customer/customer_group'), 'customer_group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('FAQ Group To Customer_Group Linkage Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'bd_faq/question'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bd_faq/question'))
    ->addColumn('question_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Group ID')
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
    ), 'Question Title')
    ->addColumn('content', Varien_Db_Ddl_Table::TYPE_TEXT, '2M', array(
        'nullable'  => false,
    ), 'Group Image')
    ->addColumn('sort_order', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
    ), 'Sort Order')
    ->addColumn('is_active', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'default'   => '1',
    ), 'Is Group Active')
    ->setComment('Bigdrop FAQ Group Table');
$installer->getConnection()->createTable($table);

/**
 * Create table 'bd_faq/question_group'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bd_faq/question_group'))
    ->addColumn('question_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'nullable'  => false,
        'primary'   => true,
    ), 'Question ID')
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'Group ID')
    ->addIndex($installer->getIdxName('bd_faq/question_group', array('group_id')),
        array('group_id'))
    ->addForeignKey($installer->getFkName('bd_faq/question_group', 'question_id', 'bd_faq/question', 'question_id'),
        'question_id', $installer->getTable('bd_faq/question'), 'question_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('bd_faq/question_group', 'group_id', 'bd_faq/group', 'group_id'),
        'group_id', $installer->getTable('bd_faq/group'), 'group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('FAQ Question To Group Linkage Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();

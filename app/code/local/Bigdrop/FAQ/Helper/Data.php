<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return mixed
     */
    public function getCustomerGroupOptionsArray()
    {
        return Mage::getResourceModel('customer/group_collection')->toOptionHash();
    }

    /**
     * @return mixed
     */
    public function getGroupOptionsArray()
    {
        return Mage::getResourceModel('bd_faq/group_collection')->toOptionHash();
    }

    /**
     * @return mixed
     */
    public function getCustomerGroupValuesArray()
    {
        return Mage::getResourceModel('customer/group_collection')->toOptionArray();
    }

    /**
     * @return mixed
     */
    public function getGroupValuesArray()
    {
        return Mage::getResourceModel('bd_faq/group_collection')->toOptionArray();
    }

    /**
     * @return string path where to save image
     */
    public function getImageDir()
    {
        return Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'bigdrop' . DS;
    }

    /**
     * if file exists return its url, either false
     * @param array|string $value
     * @return false|string
     */
    public function getImageUrl($value)
    {
        $imagePath = $this->getImageDir() . $this->getBaseImageName($value);

        if (is_file($imagePath)) {
            $baseDir = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'bigdrop' . DS;

            return is_array($value) ? $value['value'] : $baseDir.$value;
        }

        return false;
    }

    /**
     * return name of the file
     * @param array|string $value
     * @return string
     */
    private function getBaseImageName($value)
    {
        return is_array($value) ? basename($value['value']) : basename($value);
    }

    /**
     * Check whether passed property is numeric
     * @param mixed $property
     * @return bool
     */
    public function isNumeric($property)
    {
        return preg_match('/^[0-9]+$/', $property);
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     * @param array $data
     * @throws Exception
     */
    public function saveImage($model, $data)
    {
        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            $uploader = new Varien_File_Uploader('image');
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);

            $fileName = $_FILES['image']['name'];
            $path = $this->getImageDir();

            $uploader->save($path, $fileName);
            $model->setImage($uploader->getUploadedFileName());
        } elseif (isset($data['image']['delete']) && $data['image']['delete'] == 1) {
            @unlink($this->getImage($data['image']));
            $model->setImage('');
        } else {
            $model->unsImage();
        }
    }

    /**
     * get full path to image
     *
     * @param array|string $value
     * @return string
     */
    private function getImage($value)
    {
        return $this->getImageDir().$this->getBaseImageName($value);
    }
}
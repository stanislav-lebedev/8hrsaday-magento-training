<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Questions_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_questions';
        $this->_blockGroup = 'bd_faq';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('bd_faq')->__('Save Question'));
        $this->_updateButton('delete', 'label', Mage::helper('bd_faq')->__('Delete Question'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::registry('bd_faq_question')->getId() ?
            Mage::helper('bd_faq')->__("Edit Question '%s'", $this->escapeHtml(Mage::registry('bd_faq_question')->getName())) :
            Mage::helper('bd_faq')->__('New Question');
    }
}

<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Questions_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('edit_form');
        $this->setTitle(Mage::helper('bd_faq')->__('Question Information'));
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     */
    protected function _prepareForm()
    {
        /**
         * @var $model Bigdrop_FAQ_Model_Question
         */
        $model = Mage::registry('bd_faq_question');

        /**
         * @var $helper Bigdrop_FAQ_Helper_Data
         */
        $helper = Mage::helper('bd_faq');

        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', ['id' => $this->getRequest()->getParam('id')]),
                'method' => 'post',
            )
        );

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>$helper->__('General Information'), 'class' => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('question_id', 'hidden', array(
                'name' => 'question_id',
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => $helper->__('Question Title'),
            'title'     => $helper->__('Question Title'),
            'required'  => true,
        ));

        $fieldset->addField('group_id', 'multiselect', array(
            'name'      => 'groups[]',
            'label'     => $helper->__('Group'),
            'title'     => $helper->__('Group'),
            'required'  => true,
            'values'    => $helper->getGroupValuesArray(),
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => $helper->__('Status'),
            'title'     => $helper->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => $helper->__('Enabled'),
                '0' => $helper->__('Disabled'),
            ),
        ));

        $fieldset->addField('sort_order', 'text', array(
            'name'      => 'sort_order',
            'label'     => $helper->__('Sort Order'),
            'title'     => $helper->__('Sort Order'),
            'required'  => false,
            'class'     => 'validate-number',
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => $helper->__('Question Content'),
            'title'     => $helper->__('Question Content'),
            'style'     => 'height:14em',
            'required'  => true,
            'wysiwyg'   => true,
            'config'    => $wysiwygConfig
        ));

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $form->setValues(array_merge(
            $model->getData(), array('image' => $helper->getImageUrl($model->getImage()))
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}

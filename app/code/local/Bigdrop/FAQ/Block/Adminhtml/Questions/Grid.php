<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Questions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('bigdrop_faq_questions_grid');
        $this->setDefaultSort('question_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        /**
         * @var $collection Bigdrop_FAQ_Model_Resource_Question_Collection
         */
        $collection = Mage::getModel('bd_faq/question')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare columns for grid
     *
     * @return Bigdrop_FAQ_Block_Adminhtml_Questions_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('question_id', [
            'type' => 'number',
            'header'=> $this->__('ID'),
            'sortable' => true,
            'width' => '50',
            'index' => 'question_id'
        ]);

        $this->addColumn('title', [
            'header'=> $this->__('Title'),
            'sortable' => true,
            'index' => 'title'
        ]);

        $this->addColumn('content', [
            'header'=> $this->__('Content'),
            'sortable' => false,
            'index' => 'content'
        ]);

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('bd_faq')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => [
                0 => Mage::helper('bd_faq')->__('Disabled'),
                1 => Mage::helper('bd_faq')->__('Enabled')
            ]
        ));

        $this->addColumn('question_group_id', array(
            'header'        => Mage::helper('bd_faq')->__('Group'),
            'index'         => 'group_id',
            'type'          => 'options',
            'sortable'      => false,
            'options'   => Mage::helper('bd_faq')->getGroupOptionsArray(),
            'filter_condition_callback'
            => array($this, '_filterGroupCondition'),
        ));

        $this->addColumn('sort_order', [
            'header'=> $this->__('Sort Order'),
            'sortable' => false,
            'filter'    => false,
            'index' => 'sort_order'
        ]);
        return parent::_prepareColumns();
    }

    /**
     * @return Bigdrop_FAQ_Block_Adminhtml_Questions_Grid
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * Callback for filtering column by group
     *
     * @param $collection Bigdrop_FAQ_Model_Resource_Question_Collection
     * @param $column Mage_Adminhtml_Block_Widget_Grid_Column
     * @return Bigdrop_FAQ_Block_Adminhtml_Questions_Grid|void
     */
    protected function _filterGroupCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $collection->addFilter('group', $value, 'public');

        return $this;
    }

    /**
     * @param $row Bigdrop_FAQ_Model_Question
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }
}
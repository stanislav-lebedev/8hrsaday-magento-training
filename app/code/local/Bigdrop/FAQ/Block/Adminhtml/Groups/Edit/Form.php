<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Groups_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('edit_form');
        $this->setTitle(Mage::helper('bd_faq')->__('Group Information'));
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Form
     * @throws Exception
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _prepareForm()
    {
        /**
         * @var $model Bigdrop_FAQ_Model_Group
         */
        $model = Mage::registry('bd_faq_group');

        /**
         * @var $helper Bigdrop_FAQ_Helper_Data
         */
        $helper = Mage::helper('bd_faq');

        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', ['id' => $this->getRequest()->getParam('id')]),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>$helper->__('General Information'), 'class' => 'fieldset-wide'));

        if ($model->getGroupId()) {
            $fieldset->addField('group_id', 'hidden', array(
                'name' => 'group_id',
            ));
        }

        $fieldset->addField('name', 'text', array(
            'name'      => 'name',
            'label'     => $helper->__('Group Name'),
            'title'     => $helper->__('Group Name'),
            'required'  => true,
        ));

        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field =$fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => $helper->__('Store View'),
                'title'     => $helper->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $fieldset->addField('customer_group_id', 'multiselect', array(
            'name'      => 'customer_groups[]',
            'label'     => $helper->__('Customer Group'),
            'title'     => $helper->__('Customer Group'),
            'required'  => true,
            'values'    => $helper->getCustomerGroupValuesArray(),
        ));

        $fieldset->addField('image', 'image', array(
            'label' => $helper->__('Image'),
            'name' => 'image',
            'note' => '(*.jpg, *.jpeg, *.png, *.gif)',
        ));

        $fieldset->addField('is_active', 'select', array(
            'label'     => $helper->__('Status'),
            'title'     => $helper->__('Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => array(
                '1' => $helper->__('Enabled'),
                '0' => $helper->__('Disabled'),
            ),
        ));

        $fieldset->addField('sort_order', 'text', array(
            'name'      => 'sort_order',
            'label'     => $helper->__('Sort Order'),
            'title'     => $helper->__('Sort Order'),
            'required'  => false,
            'class'     => 'validate-number',
        ));

        if (!$model->getId()) {
            $model->setData('is_active', '1');
        }

        $form->setValues(array_merge(
            $model->getData(), array('image' => $helper->getImageUrl($model->getImage()))
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}

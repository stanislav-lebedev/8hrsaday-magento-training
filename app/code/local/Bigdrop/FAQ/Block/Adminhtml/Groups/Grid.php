<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Groups_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('bigdrop_faq_groups_grid');
        $this->setDefaultSort('group_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('bd_faq/group')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Bigdrop_FAQ_Block_Adminhtml_Groups_Grid
     * @throws Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn('group_id', [
            'type' => 'number',
            'header'=> $this->__('ID'),
            'sortable' => true,
            'width' => '50',
            'index' => 'group_id'
        ]);

        $this->addColumn('name', [
            'header'=> $this->__('Name'),
            'sortable' => true,
            'index' => 'name'
        ]);

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('bd_faq')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => false,
                'sortable'      => false,
                'filter_condition_callback'
                => array($this, '_filterStoreCondition'),
            ));
        }

        $this->addColumn('customer_group_id', array(
            'header'        => Mage::helper('bd_faq')->__('Customer Group'),
            'index'         => 'customer_group_id',
            'type'          => 'options',
            'sortable'      => false,
            'options'   => Mage::helper('bd_faq')->getCustomerGroupOptionsArray(),
            'filter_condition_callback'
            => array($this, '_filterCustomerGroupCondition'),
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('bd_faq')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => [
                0 => Mage::helper('bd_faq')->__('Disabled'),
                1 => Mage::helper('bd_faq')->__('Enabled')
            ]
        ));

        $this->addColumn('sort_order', [
            'header'=> $this->__('Sort Order'),
            'sortable' => false,
            'filter'    => false,
            'index' => 'sort_order'
        ]);

        return parent::_prepareColumns();
    }

    /**
     * @return Bigdrop_FAQ_Block_Adminhtml_Groups_Grid|void
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * Callback for filtering groups by store
     *
     * @param $collection Bigdrop_FAQ_Model_Resource_Group_Collection
     * @param $column Mage_Adminhtml_Block_Widget_Grid_Column
     * @return void
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $collection->addStoreFilter($value);
    }

    /**
     * @param $collection Bigdrop_FAQ_Model_Resource_Group_Collection
     * @param $column Mage_Adminhtml_Block_Widget_Grid_Column
     * @return Bigdrop_FAQ_Block_Adminhtml_Groups_Grid|void
     */
    protected function _filterCustomerGroupCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $collection->addFilter('customer_group', $value, 'public');

        return $this;
    }

    /**
     * @param $row Bigdrop_FAQ_Model_Group
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', ['id' => $row->getId()]);
    }
}
<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Questions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'bd_faq';
        $this->_controller = 'adminhtml_questions';
        $this->_headerText = $this->__('Questions');
        $this->_addButtonLabel = $this->__('Add New Question');
        parent::__construct();
    }
}
<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_Adminhtml_Groups extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'bd_faq';
        $this->_controller = 'adminhtml_groups';
        $this->_headerText = $this->__('Groups');
        $this->_addButtonLabel = $this->__('Add New Group');
        parent::__construct();
    }
}
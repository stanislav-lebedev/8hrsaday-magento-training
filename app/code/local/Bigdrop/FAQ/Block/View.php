<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Block_View extends Mage_Core_Block_Template
{
    /**
     * @var null|Bigdrop_FAQ_Model_Resource_Group_Collection
     */
    protected $groupCollection;

    /**
     * @return Bigdrop_FAQ_Model_Resource_Group_Collection|null
     */
    public function getGroupCollection()
    {
        return $this->groupCollection;
    }

    /**
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        if (!$this->groupCollection) {
            $storeID    = Mage::app()->getStore()->getId();
            $customer   = Mage::helper('customer')->getCustomer();
            $customerID = $customer->getId() ? $customer->getId() : 0;
            $groupCollection = $this->getGroupsByStoreCustomerGroup($storeID, $customerID);

            if ($groupCollection->getSize()) {
                $this->groupCollection = $groupCollection;
            }
        }

        return parent::_beforeToHtml();
    }

    /**
     * return url for arrow image
     * @return string
     */
    public function getArrowImage()
    {
        return $this->getSkinUrl('images/bigdrop/arrow.png');
    }

    /**
     * @param object $group Bigdrop_FAQ_Model_Group
     * @return string
     */
    public function getIconsPath($group)
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'bigdrop' . DS . $group->getImage();
    }

    /**
     * Return sorted collection of enabled Groups If specified for particular store and customer group
     * @param int|string $storeId
     * @param int|string $customerGroupId
     * @return Bigdrop_FAQ_Model_Resource_Group_Collection
     */
    private function getGroupsByStoreCustomerGroup($storeId = 'all', $customerGroupId = 'all')
    {
        /**
         * @var $helper Bigdrop_FAQ_Helper_Data
         */
        $helper = Mage::helper('bd_faq');

        /**
         * @var $collection Bigdrop_FAQ_Model_Resource_Group_Collection
         */
        $collection = Mage::getResourceModel('bd_faq/group_collection');

        if ($helper->isNumeric($storeId) && $storeId != 0) {
            $collection->addStoreFilter($storeId);
        }
        if ($helper->isNumeric($customerGroupId)) {
            $collection->addFilter('customer_group', $customerGroupId, 'public');
        }

        $collection->addFieldToFilter('is_active', 1)->setOrder('sort_order', 'ASC');

        return $collection;
    }

    /**
     * Return sorted collection of Questions If specified for particular group
     * @param int|string $groupId
     * @return Bigdrop_FAQ_Model_Resource_Question_Collection
     */
    public function getQuestionsByGroup($groupId = 'all')
    {
        /**
         * @var $collection Bigdrop_FAQ_Model_Resource_Question_Collection
         */
        $collection = Mage::getResourceModel('bd_faq/question_collection');

        if (Mage::helper('bd_faq')->isNumeric($groupId)) {
            $collection->addFilter('group', $groupId, 'public');
        }

        $collection->addFieldToFilter('is_active', 1)->setOrder('sort_order', 'ASC');

        return $collection;
    }
}
<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Renders FAQ Home page
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
}

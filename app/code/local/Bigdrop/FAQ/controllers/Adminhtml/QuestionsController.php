<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Adminhtml_QuestionsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Bigdrop_FAQ_Adminhtml_QuestionsController
     */
    protected function _initAction()
    {
        // load layout, set active menu
        $this->loadLayout()->_setActiveMenu('bd_faqmenu');
        return $this;
    }

    /**
     * Index action
     * Renders Layout
     */
    public function indexAction()
    {
        $this->_title($this->__('Bigdrop FAQ'))->_title($this->__('Manage Questions'));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * Create new Question
     */
    public function newAction()
    {
        // the same form is used to create and edit
        $this->_forward('edit');
    }

    /**
     * @throws Mage_Core_Exception
     * @return void
     */
    public function editAction()
    {
        /**
         * @var $helper Bigdrop_FAQ_Helper_Data
         */
        $helper = Mage::helper('bd_faq');

        $this->_title($helper->__('Bigdrop FAQ'))->_title($helper->__('Manage Question'));

        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('bd_faq/question');

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($helper->__('This question no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getTitle() : $helper->__('New Question'));

        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }

        // 4. Register model to use later in blocks
        Mage::register('bd_faq_question', $model);

        // 5. Build edit form
        $this->_initAction()->renderLayout();
    }

    /**
     *
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('bd_faq/question')->load($id);
            $helper = Mage::helper('bd_faq');

            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError($helper->__('This question no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            // try to save it
            try {
                $model->setData($data);

                // save the data
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($helper->__('The question has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Delete action
     */
    public function deleteAction()
    {
        $helper = Mage::helper('bd_faq');

        // check if we know what should be deleted
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('bd_faq/question');
                $model->load($id);
                $model->delete();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess($helper->__('The question has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;

            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError($helper->__('Unable to find a question to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }
}
<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Group extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bd_faq/group');
    }

    /**
     * delete image file when delete group
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterDelete()
    {
        @unlink(Mage::helper('bd_faq')->getImageDir() . $this->getImage());
        return parent::_afterDelete();
    }
}
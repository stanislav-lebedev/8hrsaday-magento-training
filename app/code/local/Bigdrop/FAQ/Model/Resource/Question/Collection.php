<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Resource_Question_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('bd_faq/question');
        $this->_map['fields']['group'] = 'group_table.group_id';
    }

    /**
     * Join group relation table if there is group filter
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('group')) {
            $this->getSelect()->join(
                array('group_table' => $this->getTable('bd_faq/question_group')),
                'main_table.question_id = group_table.question_id',
                array()
            );

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }

        return parent::_renderFiltersBefore();
    }
}
<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Resource_Question extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bd_faq/question', 'question_id');
    }

    /**
     * Process question data before deleting
     *
     * @param Bigdrop_FAQ_Model_Question $object
     * @return Bigdrop_FAQ_Model_Resource_Question
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $condition = array(
            'question_id = ?'     => (int) $object->getId(),
        );

        $this->_getWriteAdapter()->delete($this->getTable('bd_faq/question_group'), $condition);

        return parent::_beforeDelete($object);
    }


    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Block
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        //for stores assign
        $oldGroups = $this->lookupGroupIds($object->getId());
        $newGroups = (array)$object->getGroups();

        //if there is new or old groups
        $tableGroups  = $this->getTable('bd_faq/question_group');
        $insertGroups = array_diff($newGroups, $oldGroups);
        $deleteGroups = array_diff($oldGroups, $newGroups);

        //if there is unused old groups, delete from summary table
        if ($deleteGroups) {
            $where = array(
                'question_id = ?'     => (int) $object->getId(),
                'group_id IN (?)' => $deleteGroups
            );

            $this->_getWriteAdapter()->delete($tableGroups, $where);
        }

        //if there is unaccounted new groups, add them to summary table
        if ($insertGroups) {
            $data = array();

            foreach ($insertGroups as $groupId) {
                $data[] = array(
                    'question_id'  => (int) $object->getId(),
                    'group_id' => (int) $groupId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($tableGroups, $data);
        }

        return parent::_afterSave($object);

    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Bigdrop_FAQ_Model_Resource_Group
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $groups = $this->lookupGroupIds($object->getId());
            $object->setData('group_id', $groups);
            $object->setData('groups', $groups);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Get group ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupGroupIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('bd_faq/question_group'), 'group_id')
            ->where('question_id = :question_id');

        $binds = array(
            ':question_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }
}
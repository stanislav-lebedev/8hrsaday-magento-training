<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Resource_Group extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('bd_faq/group', 'group_id');
    }

    /**
     * Process group data before deleting
     *
     * @param Bigdrop_FAQ_Model_Group $object
     * @return Bigdrop_FAQ_Model_Resource_Group
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $condition = array(
            'group_id = ?'     => (int) $object->getId(),
        );

        $this->_getWriteAdapter()->delete($this->getTable('bd_faq/group_store'), $condition);
        $this->_getWriteAdapter()->delete($this->getTable('bd_faq/group_customer_group'), $condition);

        return parent::_beforeDelete($object);
    }


    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Cms_Model_Resource_Block
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        //for stores assign
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();

        //if there is new or old stores
        $tableStores  = $this->getTable('bd_faq/group_store');
        $insertStores = array_diff($newStores, $oldStores);
        $deleteStores = array_diff($oldStores, $newStores);

        //if there is unused old stores, delete from summary table
        if ($deleteStores) {
            $where = array(
                'group_id = ?'     => (int) $object->getId(),
                'store_id IN (?)' => $deleteStores
            );

            $this->_getWriteAdapter()->delete($tableStores, $where);
        }

        //if there is unaccounted new stores, add them to summary table
        if ($insertStores) {
            $data = array();

            foreach ($insertStores as $storeId) {
                $data[] = array(
                    'group_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($tableStores, $data);
        }


        //for customer groups assign
        $oldCustomerGroups = $this->lookupCustomerGroupIds($object->getId());
        $newCustomerGroups = (array)$object->getCustomerGroups();

        //if there is new or old customer groups
        $tableCustomerGroups  = $this->getTable('bd_faq/group_customer_group');
        $insertCustomerGroups = array_diff($newCustomerGroups, $oldCustomerGroups);
        $deleteCustomerGroups = array_diff($oldCustomerGroups, $newCustomerGroups);

        //if there is unused old CustomerGroups, delete from summary table
        if ($deleteCustomerGroups) {
            $where = array(
                'group_id = ?'     => (int) $object->getId(),
                'customer_group_id IN (?)' => $deleteCustomerGroups
            );

            $this->_getWriteAdapter()->delete($tableCustomerGroups, $where);
        }

        //if there is unaccounted new CustomerGroups, add them to summary table
        if ($insertCustomerGroups) {
            $data = array();

            foreach ($insertCustomerGroups as $customerGroupId) {
                $data[] = array(
                    'group_id'  => (int) $object->getId(),
                    'customer_group_id' => (int) $customerGroupId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($tableCustomerGroups, $data);
        }

        return parent::_afterSave($object);
    }

    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Bigdrop_FAQ_Model_Resource_Group
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $customerGroups = $this->lookupCustomerGroupIds($object->getId());
            $object->setData('store_id', $stores);
            $object->setData('stores', $stores);
            $object->setData('customer_group_id', $customerGroups);
            $object->setData('customer_groups', $customerGroups);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('bd_faq/group_store'), 'store_id')
            ->where('group_id = :group_id');

        $binds = array(
            ':group_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }

    /**
     * Get customer group ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupCustomerGroupIds($id)
    {
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('bd_faq/group_customer_group'), 'customer_group_id')
            ->where('group_id = :group_id');

        $binds = array(
            ':group_id' => (int) $id
        );

        return $adapter->fetchCol($select, $binds);
    }
}
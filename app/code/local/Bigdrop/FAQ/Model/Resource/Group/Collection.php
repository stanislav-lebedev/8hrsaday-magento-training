<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Resource_Group_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('bd_faq/group');
        $this->_map['fields']['store'] = 'store_table.store_id';
        $this->_map['fields']['customer_group'] = 'customer_group_table.customer_group_id';
    }

    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Mage_Cms_Model_Resource_Block_Collection
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if ($store instanceof Mage_Core_Model_Store) {
            $store = array($store->getId());
        }

        if (!is_array($store)) {
            $store = array($store);
        }

        if ($withAdmin) {
            $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
        }

        $this->addFilter('store', array('in' => $store), 'public');

        return $this;
    }

    /**
     * Join store and customer_group relation tables if there is store or customer_group filter
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('customer_group')) {
            $this->getSelect()->join(
                array('customer_group_table' => $this->getTable('bd_faq/group_customer_group')),
                'main_table.group_id = customer_group_table.group_id',
                array()
            );

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }

        if ($this->getFilter('store')) {
            $this->getSelect()->join(
                array('store_table' => $this->getTable('bd_faq/group_store')),
                'main_table.group_id = store_table.group_id',
                array()
            )->group('main_table.group_id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }

        return parent::_renderFiltersBefore();
    }

    /**
     * Retreive option hash
     *
     * @return array
     */
    public function toOptionHash()
    {
        return parent::_toOptionHash('group_id', 'name');
    }

    /**
     * Retreive option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        return parent::_toOptionArray('group_id');
    }
}
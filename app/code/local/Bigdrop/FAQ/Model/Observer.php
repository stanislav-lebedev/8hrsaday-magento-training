<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Observer
{
    /**
     * add Node to the Top Menu block on the frontend
     *
     * @param Varien_Event_Observer $observer
     * @event "page_block_html_topmenu_gethtml_before"
     * @return Bigdrop_FAQ_Model_Observer
     */
    public function addFaqNode(Varien_Event_Observer $observer)
    {
        $event = $observer->getEvent();

        /**
         * @var $menu Varien_Data_Tree_Node
         */
        $menu = $event->getMenu();
        $nodeData = array(
            'name'      => Mage::helper('bd_faq')->__('FAQ'),
            'id'        => 'faq_node',
            'url'       => '/faq',
            'is_active' => false
        );
        $node = new Varien_Data_Tree_Node($nodeData, 'faq_node', new Varien_Data_Tree());
        $menu->addChild($node);
        return $this;
    }
}

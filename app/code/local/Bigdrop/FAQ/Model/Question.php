<?php
/**
 * Module adds FAQ section
 *
 * @category   Bigdrop
 * @package    Bigdrop_FAQ
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_FAQ_Model_Question extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('bd_faq/question');
    }
}
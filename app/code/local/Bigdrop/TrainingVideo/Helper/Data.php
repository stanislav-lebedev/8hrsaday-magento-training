<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }


    /**
     * is customer purchased this video?
     *
     * @param int|string $videoId
     * @return bool
     */
    public function isAlreadyPurchased($videoId)
    {
        return in_array($videoId, Mage::getResourceHelper('bd_training_video')
            ->getPurchasedVideosByCustomer(Mage::getSingleton('customer/session')
            ->getCustomer()
            ->getId()));
    }

    /**
     * get video types for grid column
     * @return array
     */
    public function getVideoTypesArray()
    {
        $attribute = Mage::getModel('catalog/resource_eav_attribute')->load('type', 'attribute_code');
        $values = $attribute->getSource()->getAllOptions(false, false);

        $types = array();
        foreach ($values as $value) {
            $types[$value['value']] = $value['label'];
        }

        return $types;
    }
}
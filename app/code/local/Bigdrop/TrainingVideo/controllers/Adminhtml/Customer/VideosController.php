<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 *
 * Adminhtml customer purchased videos tab
 */

class Bigdrop_TrainingVideo_Adminhtml_Customer_VideosController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialize customer by ID specified in request
     *
     * @return Bigdrop_TrainingVideo_Adminhtml_CustomerController
     */
    protected function _initCustomer()
    {
        $customerId = (int) $this->getRequest()->getParam('id');
        $customer = Mage::getModel('customer/customer');

        if ($customerId) {
            $customer->load($customerId);
        }

        Mage::register('current_customer', $customer);
        return $this;
    }

    /**
     * Ajax action for purhcased videos
     */
    public function gridAction()
    {
        $this->_initCustomer();
        $this->loadLayout(false)
            ->renderLayout();
    }
}

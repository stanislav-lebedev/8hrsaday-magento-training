<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$productType = Bigdrop_TrainingVideo_Model_Product_Type::TYPE_TRAINING_VIDEO;
$entityType = Mage_Catalog_Model_Product::ENTITY;
$attributeSetName = 'BD Video Options';
$attributesGroup = 'BD Video Attributes';

/**
 * add new attribute set based on default
 */
// get catalog product entity type id
$entityTypeId = Mage::getModel('catalog/product')->getResource()->getTypeId();

/** @var $attributeSet Mage_Eav_Model_Entity_Attribute_Set */
$attributeSet = Mage::getModel('eav/entity_attribute_set')
    ->setEntityTypeId($entityTypeId)
    ->setAttributeSetName($attributeSetName);
    // check if name is valid
    $attributeSet->validate();
    // copy parameters to new set from skeleton set
    $attributeSet->save();
    $attributeSet->initFromSkeleton($entityTypeId)->save();

// add custom attributes to new product type
$installer->addAttribute($entityType, 'preview_video_url', array(
    'group' => $attributesGroup,
    'attribute_set' => $attributeSetName,
    'type' => 'varchar',
    'label' => 'Preview Video URL',
    'input' => 'text',
    'required' => false,
    'user_defined' => 1,
    'apply_to' => $productType
));

// add custom attributes to the product and the attribute set
$installer->addAttribute($entityType, 'main_video_url', array(
    'group' => $attributesGroup,
    'attribute_set' => $attributeSetName,
    'type' => 'text',
    'label' => 'Main Video URL',
    'input' => 'text',
    'user_defined' => 1,
    'apply_to' => $productType,
));

$installer->addAttribute($entityType, 'runtime', array(
    'group' => $attributesGroup,
    'attribute_set' => $attributeSetName,
    'type' => 'varchar',
    'label' => 'Runtime',
    'input' => 'text',
    'required' => false,
    'user_defined' => 1,
    'apply_to' => $productType
));

$installer->addAttribute($entityType, 'type', array(
    'group' => $attributesGroup,
    'attribute_set' => $attributeSetName,
    'type' => 'int',
    'label' => 'Type',
    'input' => 'select',
    'option' =>
        array (
            'values' =>
                array (
                    0 => 'Beginner',
                    1 => 'Medium',
                    2 => 'Hard',
                    3 => 'Very Hard',
                ),
        ),
    'required' => false,
    'apply_to' => $productType
));

// add prices to the product
$attributes = array(
    'price',
    'special_price',
    'special_from_date',
    'special_to_date',
    'minimal_price',
    'tax_class_id'
);

foreach ($attributes as $attributeCode) {
    $applyTo = explode(
        ',',
        $installer->getAttribute(
            $entityType,
            $attributeCode,
            'apply_to'
        )
    );

    if (!in_array($productType, $applyTo)) {
        $applyTo[] = $productType;
        $installer->updateAttribute(
            $entityType,
            $attributeCode,
            'apply_to',
            join(',', $applyTo)
        );
    }
}

/**
 * Create table 'bd_training_video/video_purchased'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('bd_training_video/video_purchased'))
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => '0',
    ), 'Customer ID')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        'default'   => '0',
    ), 'Product ID')
    ->addForeignKey($installer->getFkName('bd_training_video/video_purchased', 'customer_id', 'customer/entity', 'entity_id'),
        'customer_id', $installer->getTable('customer/entity'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_SET_NULL, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey($installer->getFkName('bd_training_video/video_purchased', 'product_id', 'catalog/product', 'entity_id'),
        'product_id', $installer->getTable('catalog/product'), 'entity_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment('BD Video purchased table');
$installer->getConnection()->createTable($table);

$installer->endSetup();
<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

//add notes for url attributes
$installer->updateAttribute('catalog_product', 'preview_video_url', 'note', 'insert youtube code of the embed video, not direct link');
$installer->updateAttribute('catalog_product', 'main_video_url', 'note', 'insert youtube code of the embed video, not direct link');

$installer->endSetup();
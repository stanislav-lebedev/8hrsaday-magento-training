<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

/** @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * add validation for runtime 0>
 */
$installer->updateAttribute('catalog_product', 'runtime', 'frontend_class', 'validate-zero-or-greater');

$installer->endSetup();
<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 *
 */

class Bigdrop_TrainingVideo_Block_Adminhtml_Customer_Grid_Column_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column as small image
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $val = Mage::helper('catalog/image')->init($row, 'small_image')->resize(100);
        $out = "<img src=". $val ." width='100px'/>";
        return $out;
    }
}
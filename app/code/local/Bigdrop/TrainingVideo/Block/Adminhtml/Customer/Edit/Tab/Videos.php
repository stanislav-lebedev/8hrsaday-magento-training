<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 *
 * Adminhtml customer purchased videos tab
 */

class Bigdrop_TrainingVideo_Block_Adminhtml_Customer_Edit_Tab_Videos
    extends Mage_Adminhtml_Block_Widget_Grid
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * set grid params
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_edit_tab_videos');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/customer_videos/grid', array('_current' => true));
    }

    /**
     * Prepare collection for grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $purchasedVideos = Mage::getResourceHelper('bd_training_video')
            ->getPurchasedVideosByCustomer(Mage::registry('current_customer')
            ->getId());

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect(array('name', 'description', 'small_image', 'runtime', 'type'))
            ->addAttributeToFilter('entity_id', array('in' => $purchasedVideos));

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Add columns to grid
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('small_image', array(
            'header'            => $this->__('Image'),
            'index'             => 'small_image',
            'type'              => 'image',
            'renderer'          => 'bd_training_video/adminhtml_customer_grid_column_renderer_image',
            'sortable'          => false,
            'filter'            => false,
        ));

        $this->addColumn('name', array(
            'header'            => $this->__('Name'),
            'index'             => 'name',
            'type'              => 'text',
            'escape'            => true
        ));

        $this->addColumn('description', array(
            'header'            => $this->__('Description'),
            'index'             => 'description',
            'type'              => 'text',
            'escape'            => true,
            'sortable'          => false,
        ));

        $this->addColumn('runtime', array(
            'header'            => $this->__('Runtime'),
            'index'             => 'runtime',
            'type'              => 'text',
            'escape'            => true
        ));

        $this->addColumn('type', array(
            'header'            => $this->__('Type'),
            'index'             => 'type',
            'type'              => 'options',
            'options'           => $this->helper('bd_training_video')->getVideoTypesArray(),
        ));

        return parent::_prepareColumns();
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Purchased Videos');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Purchased Videos');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        $customer = Mage::registry('current_customer');
        return (bool)$customer->getId();
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Defines after which tab, this tab should be rendered
     *
     * @return string
     */
    public function getAfter()
    {
        return 'tags';
    }
}

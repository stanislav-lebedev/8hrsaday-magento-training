<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Block_Pagination extends Mage_Core_Block_Template
{
    /**
     * set up collection
     */
    public function __construct()
    {
        parent::__construct();
        //set your own collection (get data from database so you can list it)
        $purchasedVideos = Mage::getResourceHelper('bd_training_video')
            ->getPurchasedVideosByCustomer(Mage::getSingleton('customer/session')
            ->getCustomer()
            ->getId());
        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect(array('name', 'description', 'main_video_url', 'runtime', 'type'))
            ->addAttributeToFilter('entity_id', array('in' => $purchasedVideos));
        $this->setCollection($collection);
    }

    /**
     * set up toolbar
     * @return $this|Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        //this is toolbar we created in the previous step
        $toolbar = $this->getLayout()->createBlock('bd_training_video/toolbar');
        //this is where you set what options would you like to  have for sorting your grid. Key is your column in your database, value is just value that will be shown in template
        $toolbar->setAvailableOrders(array('name'=> 'Title','runtime'=>'Runtime', 'type'=>'Type'));
        $toolbar->setDefaultOrder('id');
        $toolbar->setDefaultDirection("asc");
        $toolbar->setCollection($this->getCollection());
        $this->setChild('toolbar', $toolbar);
        $this->getCollection()->load();
        return $this;
    }

    /**
     * render toolbar in template
     * @return string
     */
    public function getToolbarHtml()
    {
        return $this->getChildHtml('toolbar');
    }

    /**
     * @param string $code from DB
     * @return string
     */
    public function getMainVideoUrl($code)
    {
        return 'https://www.youtube.com/embed/'. $code;
    }
}
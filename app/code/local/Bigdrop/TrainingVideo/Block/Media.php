<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Block_Media extends Mage_Catalog_Block_Product_View_Media
{
    /**
     * @return string|false
     */
    public function getPreviewVideoUrl()
    {
        return 'https://www.youtube.com/embed/'. $this->getProduct()->getPreviewVideoUrl();
    }
}
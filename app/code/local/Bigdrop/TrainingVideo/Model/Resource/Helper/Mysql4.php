<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Model_Resource_Helper_Mysql4 extends Mage_Core_Model_Resource_Helper_Mysql4
{
    /**
     * @return string
     */
    protected function getMainTable()
    {
        return Mage::getSingleton('core/resource')->getTableName('bd_training_video/video_purchased');
    }

    /**
     * @param int|string $customerId
     * @return array
     */
    public function getPurchasedVideosByCustomer($customerId)
    {
        $select = $this->_getReadAdapter()
            ->select()
            ->from($this->getMainTable(), 'product_id')
            ->where('customer_id = ?', $customerId);

        return $this->_getReadAdapter()->fetchCol($select);
    }

    /**
     * @param array $data array $data Column-value pairs or array of Column-value pairs
     */
    public function addToPurchasedVideos($data)
    {
        $this->_getWriteAdapter()->insertMultiple($this->getMainTable(), $data);
    }
}

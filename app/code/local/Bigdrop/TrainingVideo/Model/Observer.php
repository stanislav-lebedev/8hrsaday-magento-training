<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Model_Observer
{
    /**
     * set tab inventory in admin Manage product
     *
     * @param Varien_Event_Observer $observer
     * @event core_block_abstract_to_html_before
     */
    public function setDefaultInventory(Varien_Event_Observer $observer)
    {
        $request = Mage::app()->getRequest();
        $type = $request->getParam('type', false);

        if ($type != Bigdrop_TrainingVideo_Model_Product_Type::TYPE_TRAINING_VIDEO) {
            return;
        }

        $block = $observer->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Inventory) {
            $block->setTemplate('bigdrop/training_video/inventory.phtml');
        }
    }

    /**
     * throws exception if customer already bought video(s)
     *
     * @param Varien_Event_Observer $observer
     * @throws Mage_Core_Exception
     * @event sales_order_place_before
     */
    public function checkIfVideoOrderedByCustomer(Varien_Event_Observer $observer)
    {
        /**
         * @var $order Mage_Sales_Model_Order
         */
        $order = $observer->getOrder();

        if($order->getCustomerIsGuest() || !$order->getCustomerId()) {
            Mage::throwException(
                Mage::helper('sales')->__("Please log in")
            );
        } else {
            /**
             * @var Bigdrop_TrainingVideo_Model_Resource_Helper_Mysql4
             */
            $helper = Mage::getResourceHelper('bd_training_video');

            $purchasedVideos = $helper->getPurchasedVideosByCustomer($order->getCustomerId());
            $repeatingVideos= array();

            foreach ($order->getItemsCollection() as $item) {
                if (in_array($item->getProductId(), $purchasedVideos)) {
                    $repeatingVideos[] = $item->getName();
                }
            }

            if ($repeatingVideos) {
                Mage::throwException(
                    Mage::helper('sales')->__("You already bought video(s) '" . implode(", ", $repeatingVideos) . "', please remove it from the cart")
                );
            }
        }
    }

    /**
     * saves product ad customer ids to table
     *
     * @param Varien_Event_Observer $observer
     * @event sales_order_place_after
     *
     */
    public function addToPurchasedVideos(Varien_Event_Observer $observer)
    {
        /**
         * @var $order Mage_Sales_Model_Order
         */
        $order = $observer->getOrder();
        $data = array();

        foreach ($order->getItemsCollection() as $item) {
            if ($item->getProductType() === Bigdrop_TrainingVideo_Model_Product_Type::TYPE_TRAINING_VIDEO) {
                $data[] = array(
                    'customer_id' => $order->getCustomerId(),
                    'product_id' => $item->getProductId()
                );
            }
        }

        //insert to db table
        if ($data) {
            try {
                Mage::getResourceHelper('bd_training_video')->addToPurchasedVideos($data);
            } catch (Exception $e) {
                Mage::log($e->getMessage(), null, 'bigdrop_training_video.log');
            }
        }
    }
}

<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Model_Product_Type extends Mage_Catalog_Model_Product_Type_Virtual
{
    const TYPE_TRAINING_VIDEO = 'training_video';

    /**
     * set to true for replace Add To Cart button in template list.phtml
     * @param null $product
     * @return bool
     */
    public function canConfigure($product = null)
    {
        return true;
    }

    /**
     * Initialize product(s) for add to cart process.
     * Advanced version of func to prepare product for cart - processMode can be specified there.
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @param null|string $processMode
     * @return array|string
     */
    public function prepareForCartAdvanced(Varien_Object $buyRequest, $product = null, $processMode = null)
    {
        //checks if video already bought by customer
        if(Mage::getSingleton('customer/session')->isLoggedIn()) {

            if (Mage::helper('bd_training_video')->isAlreadyPurchased($product->getId())) {
                return Mage::helper('bd_training_video')->__('You already bought this video, please choose another one');
            }
            return parent::prepareForCartAdvanced($buyRequest, $product, $processMode);
        } else {
            return Mage::helper('bd_training_video')->__('Please log in');
        }
    }
}
<?php
/**
 * Module creates new product type "Training Video"
 *
 * @category   Bigdrop
 * @package    Bigdrop_TrainingVideo
 * @author     Stanislav Lebedev <stanislav.lebedev@bigdropinc.com>
 */

class Bigdrop_TrainingVideo_Model_Product_Price extends Mage_Catalog_Model_Product_Type_Price
{
}

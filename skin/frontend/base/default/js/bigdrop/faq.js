jQuery(document).ready(function(){
// Question handler
    jQuery('li.bd-faq-question').on('click', function(){

        // gets next element
        // opens .a of selected question
        jQuery(this).next().slideToggle("500")

        // selects all other answers and slides up any open answer
            .siblings('li.bd-faq-answer').slideUp();

        // Grab img from clicked question
        var img = jQuery(this).children('img');

        // remove Rotate class from all images except the active
        jQuery('img').not(img).removeClass('rotate');

        // toggle rotate class
        img.toggleClass('rotate');

    });

    jQuery('.bd-faq-button').click(function () {
        var groupId = jQuery(this).attr('data-group-id');
        jQuery('div.bd-faq')
            .filter('.group-'+groupId)
            .slideToggle();

        jQuery('div.bd-faq').filter(':not(.group-'+groupId+')').slideUp();
    });
});


